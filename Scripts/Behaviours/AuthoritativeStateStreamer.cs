﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;

namespace NetworkAuthority.Inputs
{
    /// <summary>
    /// This class is responsible for transmitting client input data to the server and receiving server
    /// game state data.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class AuthoritativeStateStreamer : NetworkComponentModule
    {
        private IStateTransceiver[] stateTransceivers;
        private Queue<PlayerInputMsg> inputBuffer;
        private Queue<GameStateMsg> gameStateBuffer;
        
        private bool rewinding;
        private List<PlayerInputMsg> rewindData;
        private List<GameStateMsg> predictionData;

        public override Type componentType => typeof(IStateTransceiver);

        /// <summary>
        /// Register network handlers.
        /// </summary>
        [RuntimeInitializeOnLoadMethod]
        [UsedImplicitly]
        internal static void RegisterHandlers()
        {
            NetworkClient.RegisterHandler<GameStateMsg>(OnGameStateReceived);
            NetworkServer.RegisterHandler<PlayerInputMsg>(OnPlayerInputReceived);
        }

        private void Awake()
        {
            stateTransceivers = GetComponents<IStateTransceiver>();
        }

        private void Update()
        {
            AuthorityUpdate();
            ObserverUpdate();
        }

        private void ObserverUpdate()
        {
            if (isServerOnly)
                return;

            if (hasAuthority)
                return;

            foreach (IStateTransceiver input in stateTransceivers) input.OnObserverUpdate();
        }

        private void AuthorityUpdate()
        {
            if (isServerOnly)
                return;

            if (!hasAuthority || rewinding)
                return;

            foreach (IStateTransceiver input in stateTransceivers) input.OnAuthorityUpdate();
        }

        private void FixedUpdate()
        {
            AuthorityFixedUpdate();
            
            UpdateInputStream();
            UpdateGameStateStream();
            ReceiveGameStateStream();
        }

        private void LateUpdate()
        {
            if (hasAuthority || isServer)
                return;
            
            foreach (IStateTransceiver input in stateTransceivers) input.OnObserverLateUpdate();
        }

        private void AuthorityFixedUpdate()
        {
            if (isServerOnly)
                return;

            if (!hasAuthority || rewinding)
                return;

            foreach (IStateTransceiver input in stateTransceivers) input.OnAuthorityFixedUpdate();
        }

        public override void OnStartServer()
        {
            // Register handler and initialize the player input queue.
            inputBuffer = new Queue<PlayerInputMsg>();
        }

        public override void OnStartClient()
        {
            // Register handler and initialize
            rewindData = new List<PlayerInputMsg>();
            predictionData = new List<GameStateMsg>();
            gameStateBuffer = new Queue<GameStateMsg>();
        }

        private static void OnPlayerInputReceived(NetworkConnection conn, PlayerInputMsg msg)
        {
            if (!NetworkServer.active)
                return;
            
            // Get network identity and then enqueue the input message.
            NetworkIdentity identity = msg.Reader.ReadNetworkIdentity();
            AuthoritativeStateStreamer input = identity.GetComponent<AuthoritativeStateStreamer>();
            input.inputBuffer.Enqueue(msg);
        }

        private static void OnGameStateReceived(NetworkConnection conn, GameStateMsg msg)
        {
            if (!NetworkClient.isConnected)
                return;
            if (!NetworkClient.connection.isReady)
                return;

            // Get network identity and then enqueue game state message.
            NetworkIdentity identity = msg.Reader.ReadNetworkIdentity();
            if (identity == null)
                return;
            
            // Get the authoritative input component from the identity object.
            AuthoritativeStateStreamer input = identity.GetComponent<AuthoritativeStateStreamer>();
            if (input == null)
                return;
            
            input.gameStateBuffer.Enqueue(msg);
        }

        private void UpdateInputStream()
        {
            if (isServerOnly)
                return;
            
            if (!hasAuthority)
                return;

            // Get the input writer.
            PlayerInputMsg inputMsg = GetInputMessage();

            // Execute the input on the client.
            ExecuteInput();

            // Only send messages when connected.
            if (isClientOnly)
            {
                // Add the input to the rewind.
                rewindData.Add(new PlayerInputMsg(inputMsg));

                // Get the predicted game state.
                GameStateMsg gameStateMsg = GetGameStateMsg(inputMsg.Time);

                // Add the game state to the prediction.
                predictionData.Add(gameStateMsg);

                // Send the input message to the server.
                NetworkClient.connection.Send(inputMsg);
            }
        }

        private void UpdateGameStateStream()
        {
            if (!isServerOnly)
                return;

            while (inputBuffer.Count > 0)
            {
                // Get the next player input message.
                PlayerInputMsg inputMsg = inputBuffer.Dequeue();

                // Read time.
                float time = inputMsg.Reader.ReadSingle();

                // Read input message.
                ReadInputData(inputMsg.Reader);

                // Execute input.
                ExecuteInput();

                // Only send data when connected.
                if (isServerOnly)
                {
                    // Get the game state.
                    GameStateMsg gameStateMsg = GetGameStateMsg(time);
                    
                    // Transmit the game state to all clients.
                    if (!NetworkServer.SendToAll(gameStateMsg))
                    {
                        Debug.Log("Failed to send input stream to all clients.");
                    }
                }
            }
        }

        private void ReceiveGameStateStream()
        {
            if (isServer)
                return;

            // Loop through the game state buffer.
            while (gameStateBuffer.Count > 0)
            {
                // Get the oldest game state.
                GameStateMsg gameState = gameStateBuffer.Dequeue();

                // Find the matching prediction packet and the rewind packets.
                GameStateMsg predictionPacket = predictionData.Find(x => x.Time == gameState.Time);
                List<PlayerInputMsg> rewindPackets = rewindData.FindAll(x => x.Time > gameState.Time);

                // Try to update the game state.
                if (!UpdateGameState(new GameStateMsg(gameState), predictionPacket) && hasAuthority)
                {
                    Debug.Log($"Client {{{NetworkClient.connection.connectionId}}} game state mismatch at frame {gameState.Time}");
                    ResetToGameState(new GameStateMsg(gameState), rewindPackets); // Reset the game state on fail.
                }

                // Remove all old prediction packets and rewind packets.
                predictionData.RemoveAll(x => x.Time <= gameState.Time);
                rewindData.RemoveAll(x => x.Time <= gameState.Time);
            }
        }

        private void ResetToGameState(InputDataMsg gameState, IEnumerable<PlayerInputMsg> rewindPackets)
        {
            // Make sure to set rewinding to true to force the update loop to stop.
            rewinding = true;

            // Read forward
            gameState.ReadForward();

            // Force game state for all inputs
            foreach (IStateTransceiver input in stateTransceivers)
            {
                input.ReadGameState(gameState.Reader, false);
                input.SetGameState();
            }

            // Loop through each rewind message and execute the input.
            foreach (PlayerInputMsg rewindMsg in rewindPackets)
            {
                // Read forward the rewind message.
                rewindMsg.ReadForward();

                foreach (IStateTransceiver input in stateTransceivers)
                {
                    // Read the input message.
                    input.ReadInputs(rewindMsg.Reader);

                    // Execute the input message.
                    input.Execute();
                }

                // Make sure to reset the rewind message for later use.
                int index = rewindData.IndexOf(rewindMsg);
                rewindData[index] = new PlayerInputMsg(rewindMsg);
            }

            // Reset the rewinding.
            rewinding = false;
        }

        private bool UpdateGameState(InputDataMsg gameState, InputDataMsg prediction)
        {
            // Read forward
            gameState.ReadForward();
            prediction?.ReadForward();

            foreach (IStateTransceiver input in stateTransceivers)
            {
                input.ReadGameState(gameState.Reader, false);
                if (prediction != null)
                    input.ReadGameState(prediction.Reader, true);
                if (!input.UpdateGameState(hasAuthority))
                    return false;
            }

            return true;
        }

        private PlayerInputMsg GetInputMessage()
        {
            // Initialize the network writer.
            NetworkWriter inputWriter = new NetworkWriter();
            inputWriter.Write(netIdentity);
            inputWriter.Write(Time.fixedTime);

            // Add input data on the client.
            AddInputData(inputWriter);
            return new PlayerInputMsg(Time.fixedTime, inputWriter.ToArray());
        }

        private GameStateMsg GetGameStateMsg(float time)
        {
            // Initialize network writer.
            NetworkWriter gameStateWriter = new NetworkWriter();
            gameStateWriter.Write(netIdentity);
            gameStateWriter.Write(time);

            // Add the game state data.
            AddGameStateData(gameStateWriter);
            return new GameStateMsg(time, gameStateWriter.ToArray());
        }

        private void AddInputData(NetworkWriter writer)
        { foreach (IStateTransceiver input in stateTransceivers) input.WriteInputs(writer); }
        private void AddGameStateData(NetworkWriter writer)
        { foreach (IStateTransceiver input in stateTransceivers) input.WriteGameState(writer); }
        private void ReadInputData(NetworkReader reader)
        { foreach (IStateTransceiver input in stateTransceivers) input.ReadInputs(reader); }
        private void ExecuteInput()
        { foreach (IStateTransceiver input in stateTransceivers) input.Execute(); }
    }
}
