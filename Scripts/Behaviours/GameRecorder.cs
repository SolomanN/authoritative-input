using System;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    [DisallowMultipleComponent]
    public class GameRecorder : NetworkComponentModule
    {
        /// <summary>
        /// The amount of time we record the game for (server only).
        /// </summary>
        [Range(1f, 5f)]
        [SerializeField] private float recordTime = 3f;

        public override Type componentType => typeof(IGameRecorder);
        
        private IGameRecorder[] gameRecorders;
        private int maxFrameBuffer;
        
        public int frame { get; set; }

        public override void OnStartServer()
        {
            maxFrameBuffer = Mathf.RoundToInt(recordTime / Time.fixedDeltaTime);
            gameRecorders = GetComponents<IGameRecorder>();
            foreach (IGameRecorder gameRecorder in gameRecorders)
                gameRecorder.Initialize(maxFrameBuffer);
        }
        
        private void FixedUpdate()
        {
            if (!isServer)
                return;

            if (frame < maxFrameBuffer)
                frame++;
            
            Record();
        }

        public virtual void Record()
        {
            foreach (IGameRecorder recorder in gameRecorders)
                recorder.Record();
        }

        public virtual void SetFrame(int frame)
        {
            foreach (IGameRecorder recorder in gameRecorders)
                recorder.SetFrame(frame);
        }

        public virtual void PrepareFrameSet()
        {
            foreach (IGameRecorder recorder in gameRecorders)
                recorder.PrepareFrameSet();
        }

        public virtual void ResetPrep()
        {
            foreach (IGameRecorder recorder in gameRecorders)
                recorder.ResetPreparation();
        }

        public virtual void ResetToFrame(int frame)
        {
            foreach (IGameRecorder recorder in gameRecorders)
            {
                recorder.SetFrame(frame);
                recorder.ResetPreparation();
            }
        }

    }
}