using CircularBuffer;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <summary>
    /// A base class for types who record game information for replay.
    /// </summary>
    /// <typeparam name="TData">The type of data to record.</typeparam>
    public abstract class GameRecorderBase<TData> : MonoBehaviour, IGameRecorder
    {
        protected CircularBuffer<TData> playbackBuffer;

        /// <summary>
        /// Initialize the playback buffer.
        /// </summary>
        /// <param name="maxBufferSize">The maximum size of the playback buffer.</param>
        public void Initialize(int maxBufferSize)
        {
            playbackBuffer = new CircularBuffer<TData>(maxBufferSize);
        }
        
        /// <summary>
        /// Records the given frame.
        /// </summary>
        public void Record()
        {
            playbackBuffer.PushFront(GetRecording());
        }

        /// <summary>
        /// Sets the playback to the given frame.
        /// </summary>
        /// <param name="frame">The frame.</param>
        public void SetFrame(int frame)
        {
            SetRecording(playbackBuffer[frame]);
        }

        /// <summary>
        /// Prepares the recorder before <see cref="SetFrame"/> is called.
        /// </summary>
        public abstract void PrepareFrameSet();

        /// <summary>
        /// Resets any preparations we made with <see cref="PrepareFrameSet"/>.
        /// </summary>
        public abstract void ResetPreparation();

        /// <summary>
        /// Returns a recording.
        /// </summary>
        /// <returns></returns>
        public abstract TData GetRecording();
        
        /// <summary>
        /// Sets the current playback to the given recording.
        /// </summary>
        /// <param name="recording"></param>
        public abstract void SetRecording(TData recording);
    }
}