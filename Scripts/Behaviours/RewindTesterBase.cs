using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <summary>
    /// A base class for types that are responsible for managing rewind tests.
    /// </summary>
    /// <typeparam name="TRewindTest"></typeparam>
    [RequireComponent(typeof(RewindManager))]
    public abstract class RewindTesterBase<TRewindTest> : MonoBehaviour, IRewindTester where TRewindTest : struct, IRewindTest
    {
        /// <summary>
        /// The rewind manager component.
        /// </summary>
        public RewindManager rewindManager { get; private set; }

        protected virtual void Awake()
        {
            rewindManager = GetComponent<RewindManager>();
        }

        /// <summary>
        /// Reads the rewind test.
        /// </summary>
        /// <param name="data">The test raw byte data.</param>
        /// <returns></returns>
        public IRewindTest ReadTest(byte[] data)
        {
            var test = new TRewindTest();
            test.ReadBytes(data);
            return test;
        }

        /// <summary>
        /// Executed when a test has succeeded.
        /// </summary>
        /// <param name="test">The test that succeeded.</param>
        public void OnTestSucceeded(IRewindTest test)
        {
            TestPassed((TRewindTest)test);
        }

        public void OnTestFailed(IRewindTest test)
        {
            TestFailed((TRewindTest)test);
        }

        /// <summary>
        /// Executed once a test succeeded on the server.
        /// </summary>
        /// <param name="test"></param>
        public abstract void TestPassed(TRewindTest test);

        /// <summary>
        /// Executed once a test failed on the server.
        /// </summary>
        /// <param name="test"></param>
        public abstract void TestFailed(TRewindTest test);
    }
}