﻿using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace NetworkAuthority
{
    /// <summary>
    /// This class is responsible for spawning the player into the game world.
    /// </summary>
    public class PlayerSpawner : NetworkBehaviour
    {
        [SerializeField] protected GameObject playerPrefab = null;

        private NetworkIdentity currentPlayer;
        
        public static PlayerSpawner instance { get; private set; }

        private static readonly Dictionary<NetworkConnection, PlayerSpawner> spawnConnections = new Dictionary<NetworkConnection, PlayerSpawner>();

        public virtual void GetPlayerSpawn(out Vector3 position, out Quaternion rotation)
        {
            position = Vector3.zero;
            rotation = Quaternion.identity;
        }

        public override void OnStartLocalPlayer()
        {
            instance = this;
        }

        public override void OnStartServer()
        {
            SpawnPlayer();
            spawnConnections[connectionToClient] = this;
        }

        private void OnDisable()
        {
            if (connectionToClient == null) return;
            spawnConnections.Remove(connectionToClient);
        }

        private void SpawnPlayer()
        {
            GetPlayerSpawn(out Vector3 pos, out Quaternion rot);
            
            GameObject inst = Instantiate(playerPrefab, pos, rot);
            currentPlayer = inst.GetComponent<NetworkIdentity>();
            inst.name = $"Player {connectionToClient.connectionId}";
            
            if (NetworkServer.SpawnWithClientAuthority(inst, gameObject))
            {
                Debug.Log($"Spawned player {connectionToClient.connectionId}");
            }
            else
            {
                Debug.Log($"Could not spawn player for client {connectionToClient.connectionId}");
                Destroy(inst);
            }
        }

        /// <summary>
        /// Respawn the player.
        /// </summary>
        public virtual bool Respawn()
        {
            if (!isServer)
                return false;
            
            if (currentPlayer == null)
                return false;

            if (currentPlayer.RemoveClientAuthority(currentPlayer.clientAuthorityOwner))
            {
                SpawnPlayer();
                return true;
            }
            else
            {
                Debug.Log("PlayerSpawner: Failed to respawn because authority couldn't be removed from initial player.");
            }

            return false;
        }

        public static PlayerSpawner GetSpawner(NetworkConnection connection)
        {
            return spawnConnections[connection];
        }
    }
}
