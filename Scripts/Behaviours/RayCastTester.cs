using System;
using System.Collections.Generic;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <summary>
    /// Responsible for managing rewindable ray casts on the server.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class RayCastTester : RewindTesterBase<RewindRayCast>
    {
        public delegate void RayTestPassed(RewindRayCast data);
        
        private static readonly Dictionary<Guid, RayTestPassed> successFunctions = new Dictionary<Guid, RayTestPassed>();
        
        public override void TestPassed(RewindRayCast test)
        {
            // Execute the success callback.
            if (successFunctions.TryGetValue(test.id, out var del)) del?.Invoke(test);
            
            // Execute a abstract callback for the ray type.
            OnHit(test);
        }

        public override void TestFailed(RewindRayCast test)
        {
            // Remove the success callback since this failed.
            if (successFunctions.ContainsKey(test.id)) successFunctions.Remove(test.id);
        }

        public abstract void OnHit(RewindRayCast cast);

        /// <summary>
        /// Executes a ray cast test on the server with rewind.
        /// </summary>
        /// <param name="sender">The network identity of the ray cast sender.</param>
        /// <param name="ray">The ray we use for the ray cast.</param>
        /// <param name="hit">Returns hit information.</param>
        /// <param name="maxDistance">The maximum distance of the ray.</param>
        /// <param name="layerMask">The ray's layer mask information.</param>
        /// <param name="interaction">The type of trigger interaction this ray has.</param>
        /// <param name="passedCallback">A callback executed on the client when this ray succeeds.</param>
        /// <typeparam name="TRayTester">The type of ray cast tester. Must be of type <see cref="RayCastTester"/></typeparam>
        /// <returns></returns>
        public static bool RayCast<TRayTester>(RewindManager sender, Ray ray, out RaycastHit hit, float maxDistance, int layerMask = Physics.DefaultRaycastLayers, QueryTriggerInteraction interaction = QueryTriggerInteraction.UseGlobal, RayTestPassed passedCallback = null) where TRayTester : RayCastTester
        {
            hit = default(RaycastHit);
         
            if (typeof(TRayTester).IsAbstract)
            {
                Debug.Log("Given TRayTester must be a non-abstract derivative of RayCastTester.");
                return false;
            }

            // Do the actual ray cast.
            if (Physics.Raycast(ray, out hit, maxDistance, layerMask, interaction))
            {
                // Make sure we hit a hit box.
                HitBox hitBox = hit.collider.GetComponent<HitBox>();
                if (hitBox == null) return false;
                
                // Get the rewind manager and tester.
                RewindManager target = hitBox.GetComponentInParent<RewindManager>();
                if (target == null) return false;
                TRayTester tester = hitBox.GetComponentInParent<TRayTester>();
                if (tester == null) return false;

                // Package the test and send a request.
                RewindRayCast test = new RewindRayCast(sender.gameObject, target.gameObject, hitBox, ray, hit, layerMask, maxDistance, interaction);
                sender.RequestTest(target, tester, test);
                if (passedCallback != null) successFunctions[test.id] = passedCallback;
                return true;
            }

            return false;
        }
    }
}