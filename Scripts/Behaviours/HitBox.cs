using System;
using System.Collections.Generic;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    [DisallowMultipleComponent]
    public class HitBox : MonoBehaviour
    {
        /// <summary>
        /// The hit box child index.
        /// </summary>
        public int index { get; private set; }
        
        /// <summary>
        /// The gameObject owner of this hit box.
        /// </summary>
        public GameObject owner { get; private set; }

        /// <summary>
        /// An internal cache of hit boxes.
        /// </summary>
        private static Dictionary<GameObject, List<HitBox>> _hitBoxes;

        /// <summary>
        /// Initialize this hit box.
        /// </summary>
        /// <param name="owner">The owner of the hit box.</param>
        /// <param name="index">The hit box child index.</param>
        public void Init(GameObject owner, int index)
        {
            this.index = index;
            this.owner = owner;

            if (_hitBoxes == null)
                _hitBoxes = new Dictionary<GameObject, List<HitBox>>();

            if (_hitBoxes.TryGetValue(owner, out List<HitBox> hitBoxes))
            {
                if (hitBoxes == null)
                    _hitBoxes[owner] = new List<HitBox>();
                _hitBoxes[owner].Add(this);
            }
            else
            {
                _hitBoxes[owner] = new List<HitBox> {this};
            }
        }

        /// <summary>
        /// De-initialize this hit box.
        /// </summary>
        public void DeInit()
        {
            _hitBoxes[owner].Remove(this);
            if (_hitBoxes[owner].Count <= 0)
                _hitBoxes.Remove(owner);
        }

        /// <summary>
        /// Grab a hit box from the internal cache.
        /// </summary>
        /// <param name="owner">The owner of the hit box.</param>
        /// <param name="index">The hit box child index.</param>
        /// <returns></returns>
        public static HitBox GetHitBox(GameObject owner, int index)
        {
            try
            {
                if (_hitBoxes.TryGetValue(owner, out List<HitBox> hb))
                {
                    if (hb != null) return hb[index];
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return null;
        }
    }
}