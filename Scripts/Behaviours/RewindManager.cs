using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <summary>
    /// Responsible for managing network rewindable tests and game recorders.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(GameRecorder))]
    public class RewindManager : NetworkComponentModule
    {
        public override Type componentType => typeof(IRewindTester);
        
        private Dictionary<IRewindTester, int> testerToIndex;
        private Dictionary<int, IRewindTester> indexToTester;

        private GameRecorder gameRecorder;

        private void Awake()
        {
            IRewindTester[] testerComponents = GetComponents<IRewindTester>();
            if (testerComponents.Length <= 0)
            {
                Debug.Log("ServerRewindController: Deactivating rewind controller because no testers where found.");
                enabled = false;
                return;
            }
            
            testerToIndex = new Dictionary<IRewindTester, int>(testerComponents.Length);
            indexToTester = new Dictionary<int, IRewindTester>(testerComponents.Length);
            for (int i = 0; i < testerComponents.Length; i++)
            {
                testerToIndex[testerComponents[i]] = i;
                indexToTester[i] = testerComponents[i];
            }
        }

        public override void OnStartServer()
        {
            gameRecorder = GetComponent<GameRecorder>();
        }

        [Command]
        private void Cmd_ServerReceiveTestRequest(NetworkIdentity target, short targetTesterIndex, byte[] data, float ping)
        {
            // Client authority was removed before receiving this rpc.
            if (target.clientAuthorityOwner == null) return;
            
            // Get the target rewind manager.
            RewindManager targetManager = target.GetComponent<RewindManager>();
            if (targetManager == null) return;
            
            // Execute the rewind test on the target.
            targetManager.ExecuteTest(this, ping, targetTesterIndex, data);
        }

        // This is where we execute the test on the receiver (the one who we want to rewind).
        public void ExecuteTest(RewindManager sender, float ping, short targetTesterIndex, byte[] data)
        {
            // Ensure valid test initialization.
            IRewindTester tester = GetTester(targetTesterIndex);
            if (tester == null) return;
            IRewindTest test = tester.ReadTest(data);
            if (test == null) return;

            // Init vars.
            int rewindFrames = Mathf.RoundToInt((ping / Time.fixedDeltaTime) * 1000 * 2);
            int startFrame = gameRecorder.frame - 1;
            int endFrame = Mathf.Max(startFrame - rewindFrames, 0);
            bool pass = false;
            
            // Prepare the test using the tester.
            gameRecorder.PrepareFrameSet();
            
            // Do a rewind to see if our test succeeded.
            for (int frame = startFrame; frame >= endFrame; frame--)
            {
                // Set frame and execute test.
                gameRecorder.SetFrame(frame);
                if (test.Test())
                {
                    pass = true;
                    break;
                }
            }

            // Reset the test.
            gameRecorder.ResetToFrame(startFrame);

            // Execute the final callback on the server immediately.
            if (!pass) OnRewindFail(tester, test);
            else OnRewindSuccess(tester, test);
            
            // Send a callback back to the client.
            TargetRpc_RewindFinished(sender.netIdentity.clientAuthorityOwner, targetTesterIndex, data, pass);
        }

        [TargetRpc]
        private void TargetRpc_RewindFinished(NetworkConnection connection, short testerIndex, byte[] testData, bool pass)
        {
            // Read the rewind message data.
            IRewindTester tester = GetTester(testerIndex);
            IRewindTest test = tester.ReadTest(testData);
            
            // Execute our final callback on the client.
            if (pass) OnRewindSuccess(tester, test);
            else OnRewindFail(tester, test);
        }

        protected virtual void OnRewindSuccess(IRewindTester tester, IRewindTest test) => tester.OnTestSucceeded(test);
        protected virtual void OnRewindFail(IRewindTester tester, IRewindTest test) => tester.OnTestFailed(test);

        /// <summary>
        /// Request a rewind test on the server.
        /// </summary>
        /// <param name="target">The target to rewind.</param>
        /// <param name="targetTester">The target rewind tester.</param>
        /// <param name="test">The rewind test to do.</param>
        public void RequestTest(RewindManager target, IRewindTester targetTester, IRewindTest test)
        {
            // Verify the test.
            if (isServerOnly) return;
            if (!hasAuthority) return;
            if (target == null) return;
            if (targetTester == null) return;
            if (test == null) return;
            if (!Time.inFixedTimeStep)
            {
                Debug.Log("RewindManager: Rewind requests must be made in a fixed time step.");
                return;
            }

            // Serialize the test and the tester.
            byte[] data = test.GetBytes();
            short testerIndex = (short)target.GetTesterIndex(targetTester);
            
            // Send a command for a test request.
            Cmd_ServerReceiveTestRequest(target.netIdentity, testerIndex, data, (float)NetworkTime.rtt);
        }

        /// <summary>
        /// Get the rewind tester by the given index.
        /// </summary>
        /// <param name="index">The rewind tester index.</param>
        /// <returns></returns>
        public IRewindTester GetTester(int index)
        {
            return indexToTester[index];
        }

        /// <summary>
        /// Get the rewind tester's index.
        /// </summary>
        /// <param name="tester">The rewind tester.</param>
        /// <returns></returns>
        public int GetTesterIndex(IRewindTester tester)
        {
            return testerToIndex[tester];
        }
    }
}