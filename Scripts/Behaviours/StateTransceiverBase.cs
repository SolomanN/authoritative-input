using Mirror;
using UnityEngine;

namespace NetworkAuthority.Inputs 
{
    public abstract class StateTransceiverBase<TInput, TGameState> : MonoBehaviour, IStateTransceiver where TInput : struct, IMessageBase where TGameState : struct, IMessageBase
    {
        private TInput currentInput;
        private TGameState currentGameState = new TGameState();
        private TGameState predictedGameState = new TGameState();
    
        protected abstract bool CompareGameState(TGameState gameState, TGameState prediction);
        protected abstract void ForceGameState(TGameState gameState);
        protected abstract void ObserverGameStateReceived(TGameState gameState);
        protected abstract void ExecuteInput(TInput input);
        protected abstract TInput GetUserInput();
        protected abstract TGameState GetGameState(TInput lastExecutedInput);
        protected virtual void ObserverUpdate() {}
        protected virtual void ObserverLateUpdate() {}

        public bool UpdateGameState(bool authority)
        {
            if (authority)
            {
                return CompareGameState(currentGameState, predictedGameState);
            }
            else
            {
                ObserverGameStateReceived(currentGameState);
                return true;
            }
        }

        public void SetGameState()
        {
            ForceGameState(currentGameState);
        }

        public void ReadInputs(NetworkReader reader)
        {
            currentInput.Deserialize(reader);
        }
    
        public void WriteInputs(NetworkWriter writer)
        {
            currentInput.Serialize(writer);
        }
    
        public void ReadGameState(NetworkReader reader, bool prediction)
        {
            if (prediction)
            {
                predictedGameState.Deserialize(reader);
            }
            else
            {
                currentGameState.Deserialize(reader);
            }
        }
    
        public void WriteGameState(NetworkWriter writer)
        {
            TGameState state = GetGameState(currentInput);
            state.Serialize(writer);
        }
    
        public void Execute()
        {
            ExecuteInput(currentInput);
        }

        public void OnAuthorityUpdate()
        {
            currentInput = GetUserInput();
        }

        public void OnAuthorityFixedUpdate()
        {
            // TODO: Not sure if we use this or not. We probably shouldn't rely on this because
            // this probably indicates we're trying to send game state information to the server
            // when that's not our responsibility.
        }
    
        public void OnObserverUpdate()
        {
            ObserverUpdate();
        }

        public void OnObserverLateUpdate()
        {
            ObserverLateUpdate();
        }
    }
}