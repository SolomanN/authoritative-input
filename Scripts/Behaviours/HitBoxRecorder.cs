using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <inheritdoc />
    /// <summary>
    /// Records hit box positions and rotations for eventual rewind.
    /// </summary>
    [DisallowMultipleComponent]
    public class HitBoxRecorder : GameRecorderBase<HitBoxRecording>
    {
        private HitBox[] hitBoxes;
        private Vector3[] positions;
        private Quaternion[] rotations;

        private Animator animator;
        private bool wasAnimatorEnabled;

        private void Awake()
        {
            hitBoxes = GetComponentsInChildren<HitBox>();
            for (int i = 0; i < hitBoxes.Length; i++)
                hitBoxes[i].Init(gameObject, i);
            
            // The animator will be used in this case so that
            // when we update the positions of the hit boxes
            // they won't get overriden by the animator.
            animator = GetComponent<Animator>();
        }

        private void OnDestroy()
        {
            foreach (HitBox hb in hitBoxes)
                hb.DeInit();
        }

        public HitBox GetHitBox(int index)
        {
            return hitBoxes[index];
        }

        public override void PrepareFrameSet()
        {
            // Disable the animator.
            if (animator != null)
            {
                wasAnimatorEnabled = animator.enabled;
                animator.enabled = false;
            }
        }

        public override void ResetPreparation()
        {
            // Set the animator back to it's initial state.
            if (animator != null) animator.enabled = wasAnimatorEnabled;
        }

        public override HitBoxRecording GetRecording()
        {
            // Record all positions and rotations.
            positions = new Vector3[hitBoxes.Length];
            rotations = new Quaternion[hitBoxes.Length];
            
            for (int i = 0; i < hitBoxes.Length; i++)
            {
                var hb = hitBoxes[i];
                var trans = hb.transform;
                positions[i] = trans.position;
                rotations[i] = trans.rotation;
            }
            
            return new HitBoxRecording { positions = positions, rotations = rotations };
        }

        public override void SetRecording(HitBoxRecording recording)
        {
            // Update the hit box transforms.
            for (int i = 0; i < hitBoxes.Length; i++)
            {
                var hbTrans = hitBoxes[i].transform;
                hbTrans.position = recording.positions[i];
                hbTrans.rotation = recording.rotations[i];
            }
            
            // Synchronize the transforms immediately.
            Physics.SyncTransforms();
        }
    }

    public class HitBoxRecording
    {
        public Vector3[] positions;
        public Quaternion[] rotations;
    }
}