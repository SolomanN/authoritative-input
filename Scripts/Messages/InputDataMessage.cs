﻿using JetBrains.Annotations;
using Mirror;

namespace NetworkAuthority.Inputs
{
    /// <summary>
    /// This class is responsible for holding player input information as a network message.
    /// </summary>
    public class InputDataMsg : MessageBase
    {
        private byte[] data;

        public InputDataMsg() { }

        public InputDataMsg(float time, byte[] data)
        {
            Time = time;
            this.data = data;
            Reader = new NetworkReader(data);
        }

        public InputDataMsg(InputDataMsg other)
        {
            Time = other.Time;
            data = other.data;
            Reader = new NetworkReader(data);
        }

        public float Time { get; private set; }
        public NetworkReader Reader { get; private set; }

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Time);
            writer.WriteBytesAndSize(data);
        }

        public override void Deserialize(NetworkReader reader)
        {
            Time = reader.ReadSingle();
            data = reader.ReadBytesAndSize();
            Reader = new NetworkReader(data);
        }

        /// <summary>
        /// Read forward the game state to the actual byte data. (Skips network identity and timestamp)
        /// </summary>
        public void ReadForward()
        {
            Reader.ReadNetworkIdentity();
            Reader.ReadSingle();
        }
    }

    public class PlayerInputMsg : InputDataMsg
    {
        [UsedImplicitly]
        public PlayerInputMsg() { }
        public PlayerInputMsg(float time, byte[] data) : base(time, data) { }
        public PlayerInputMsg(InputDataMsg other) : base(other) { }
    }

    public class GameStateMsg : InputDataMsg
    {
        [UsedImplicitly]
        public GameStateMsg() { }
        public GameStateMsg(float time, byte[] data) : base(time, data) { }
        public GameStateMsg(InputDataMsg other) : base(other) { }
    }
}
