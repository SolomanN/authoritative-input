using UnityEngine;

namespace NetworkAuthority.Utils
{
    /// <summary>
    /// A basic implementation of dead reckoning for Vector3.
    /// </summary>
    public class DeadReckoning
    {
        private Vector3 targetVector;
        private Vector3 lastDelta;
        private float lastTimeDelta;
        
        /// <summary>
        /// Gets a new vector from the last received vector that is interpolated and extrapolated.
        /// </summary>
        /// <param name="currentVector">The current vector.</param>
        /// <param name="snapDist">The snap distance (will force the current vector to last received)</param>
        /// <returns></returns>
        public Vector3 GetNewVector(Vector3 currentVector, float snapDist = 1)
        {
            if (float.IsNaN(lastTimeDelta)) return targetVector;
            
            Vector3 extrapolate = currentVector + lastDelta * lastTimeDelta;
            if (extrapolate == targetVector) return targetVector;
            
            Vector3 deltaVelocity = (extrapolate - targetVector) / lastTimeDelta;
            if (deltaVelocity.sqrMagnitude == 0) return targetVector;
            if (float.IsNaN(deltaVelocity.sqrMagnitude)) return targetVector;
            
            Vector3 interpolate = Vector3.SmoothDamp(extrapolate, targetVector, ref deltaVelocity, lastTimeDelta);
            if (interpolate.sqrMagnitude == 0) return targetVector;
            if (float.IsNaN(interpolate.sqrMagnitude)) return targetVector;
            
            return Vector3.SqrMagnitude(interpolate - targetVector) > snapDist ? targetVector : interpolate;
        }

        /// <summary>
        /// Called when we receive a new vector.
        /// </summary>
        /// <param name="vector">The new vector.</param>
        /// <param name="deltaTime">The time delta between last received and current.</param>
        public void Receive(Vector3 vector, float deltaTime)
        {
            lastTimeDelta = deltaTime;
            lastDelta = (vector - targetVector) / deltaTime;
            targetVector = vector;
        }
    }
}