using Mirror;
using NetworkAuthority.Rewind;

namespace NetworkAuthority
{
    public interface IGameRecorder
    {
        void Record();
        void SetFrame(int frame);
        void PrepareFrameSet();
        void ResetPreparation();
        void Initialize(int maxBufferSize);
    }
    
    /// <summary>
    /// This interface defines a layout for types which want to add custom data to player input and game state
    /// information.
    /// </summary>
    public interface IStateTransceiver
    {
        bool UpdateGameState(bool authority);
        void SetGameState();
        void ReadInputs(NetworkReader reader);
        void WriteInputs(NetworkWriter writer);
        void ReadGameState(NetworkReader reader, bool prediction);
        void WriteGameState(NetworkWriter writer);
        void Execute();

        void OnAuthorityUpdate();
        void OnAuthorityFixedUpdate();
        void OnObserverUpdate();
        void OnObserverLateUpdate();
    }

    public interface IByteReadWrite
    {
        byte[] GetBytes();
        void ReadBytes(byte[] bytes);
    }

    public interface IRewindTest : IByteReadWrite
    {
        bool Test();
    }

    public interface IRewindTester
    {
        IRewindTest ReadTest(byte[] data);
        void OnTestSucceeded(IRewindTest test);
        void OnTestFailed(IRewindTest test);
    }
}