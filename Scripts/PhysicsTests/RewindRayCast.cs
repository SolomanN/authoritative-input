using System;
using Mirror;
using UnityEngine;

namespace NetworkAuthority.Rewind
{
    /// <summary>
    /// A structure that contains information about a rewindable ray cast.
    /// </summary>
    public struct RewindRayCast : IRewindTest
    {
        public GameObject sender;
        public GameObject receiver;
        public HitBox hitBox;
        public Ray ray;
        public Vector3 point;
        public Vector3 normal;
        public int layerMask;
        public float maxDistance;
        public QueryTriggerInteraction interaction;
        public Guid id;
        
        private static readonly RaycastHit[] hitsNonAlloc = new RaycastHit[5];

        public RewindRayCast(GameObject sender, GameObject receiver, HitBox hitBox, Ray ray, RaycastHit hit,
            int layerMask, float maxDistance, QueryTriggerInteraction interaction)
        {
            this.sender = sender;
            this.receiver = receiver;
            this.hitBox = hitBox;
            this.ray = ray;
            this.layerMask = layerMask;
            this.maxDistance = maxDistance;
            this.interaction = interaction;
            point = hit.point;
            normal = hit.normal;
            id = Guid.NewGuid();
        }
        
        public byte[] GetBytes()
        {
            NetworkWriter writer = new NetworkWriter();
            writer.Write(sender);
            writer.Write(receiver);
            writer.Write((short)hitBox.index);
            writer.Write(ray);
            writer.Write(point);
            ushort packedNormal = FloatBytePacker.PackThreeFloatsIntoUShort(normal.x, normal.y, normal.z, 0, 1);
            writer.Write(packedNormal);
            writer.WritePackedInt32(layerMask);
            writer.Write(maxDistance);
            writer.Write((short)interaction);
            writer.Write(id);
            return writer.ToArray();
        }

        public void ReadBytes(byte[] bytes)
        {
            NetworkReader reader = new NetworkReader(bytes);
            sender = reader.ReadGameObject();
            receiver = reader.ReadGameObject();
            hitBox = GetHitBox(reader.ReadInt16());
            ray = reader.ReadRay();
            point = reader.ReadVector3();
            ushort packedNormal = reader.ReadUInt16();
            float[] floats = FloatBytePacker.UnpackUShortIntoThreeFloats(packedNormal, 0, 1); 
            normal = new Vector3(floats[0], floats[1], floats[2]);
            layerMask = reader.ReadPackedInt32();
            maxDistance = reader.ReadSingle();
            interaction = (QueryTriggerInteraction)reader.ReadInt16();
            id = reader.ReadGuid();
        }

        public HitBox GetHitBox(int index)
        {
            return HitBox.GetHitBox(receiver.gameObject, index);
        }

        public bool Test()
        {
            if (hitBox == null)
                return false;
            
            int size = Physics.RaycastNonAlloc(ray, hitsNonAlloc, maxDistance, layerMask, interaction);
            
            for (int i = 0; i < size; i++)
            {
                RaycastHit hit = hitsNonAlloc[i];
                HitBox hb = hit.collider.GetComponent<HitBox>();
                if (hb == hitBox)
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}